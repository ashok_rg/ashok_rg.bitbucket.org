var data = {
	"dailyChart" : {

	}
};

function renderDailyChart(){
	$("#dailyChart").highcharts({
		chart: { type: 'line', marginBottom: 40, borderWidth: 0},
		title: { text: '' },
		xAxis: {
			type: 'datetime'
		},
		yAxis: {
			min: 0,
			title: {
				text: "Clicks"
			}
		},
		series: [{
	        type: 'area',
	        name: 'Clicks',
	        pointInterval: 24 * 3600 * 1000,
	        pointStart: Date.UTC(2014, 8, 1),
	        data: [
	        	22, 10, 21, 4, 16, 6, 7, 28, 14, 6, 20, 11, 21, 20, 1, 1, 3, 17, 28, 19, 2, 8, 25, 3, 9, 24, 16, 26, 28, 18
	        ]
        }],
		plotOptions: {
			series: {
				enableMouseTracking: true,
				shadow: false,
				animation: false
			} 
		},
		legend: {
			verticalAlign: 'top',
			borderColor: "#ddd",
			itemStyle: {
				color: "#888"
			}
		},
		credits: { enabled: false },
	});    
}

function renderCostChart(){
	$("#costChart").highcharts({
		chart: { type: 'column', marginBottom: 40, borderWidth: 0},
		title: { text: '' },
		xAxis: {
			type: 'datetime'
		},
		yAxis: {
			min: 0,
			title: {
				text: "Cost"
			}
		},
		series: [{
	        name: 'Cost',
	        pointInterval: 24 * 3600 * 1000 * 30,
	        pointStart: Date.UTC(2014, 1, 1),
	        data: [
	        	2543, 1705, 2638, 2210, 2020, 1941
	        ]
        }],
		plotOptions: {
			series: {
				enableMouseTracking: true,
				shadow: false,
				animation: false
			} 
		},
		legend: {
			verticalAlign: 'top',
			borderColor: "#ddd",
			itemStyle: {
				color: "#888"
			}
		},
		credits: { enabled: false },
	});    
}

function renderCPCChart(){
	$("#cpcChart").highcharts({
		chart: { type: 'column', marginBottom: 40, borderWidth: 0},
		title: { text: '' },
		xAxis: {
			type: 'datetime'
		},
		yAxis: {
			min: 0,
			title: {
				text: "Average CPC"
			}
		},
		series: [{
	        name: 'Average CPC',
	        pointInterval: 24 * 3600 * 1000 * 30,
	        pointStart: Date.UTC(2014, 1, 1),
	        data: [
	        	0.73, 0.71, 0.62, 0.65, 0.64, 0.59
	        ]
        }],
		plotOptions: {
			series: {
				enableMouseTracking: true,
				shadow: false,
				animation: false
			} 
		},
		legend: {
			verticalAlign: 'top',
			borderColor: "#ddd",
			itemStyle: {
				color: "#888"
			}
		},
		credits: { enabled: false },
	});    
}

function renderNetworkChart(){
	$("#networkPerfChart").highcharts({
		chart: { type: 'pie', marginBottom: 0, borderWidth: 0},
		title: { text: '' },
		yAxis: {
			min: 0,
			title: {
				text: "Clicks"
			}
		},
		series: [{
            name: 'Clicks',
            data: [
            ["Search", 49],
			["Display", 32],
			["Search Partners", 23]]
        }],
		plotOptions: {
			pie: {
                borderColor: '#000000',
                innerSize: '60%'
            }
		},
		legend: {
			verticalAlign: 'top',
			borderColor: "#ddd",
			itemStyle: {
				color: "#888"
			}
		},
		credits: { enabled: false },
	});    
}

function renderCampaignChart(){
	$("#campaignPerfChart").highcharts({
		chart: { type: 'column', marginBottom: 40, borderWidth: 0},
		title: { text: '' },
		xAxis: {
			categories: [
				"Campaign #1",
				"Campaign #2",
				"Campaign #3",
			]
		},
		yAxis: {
			min: 0,
			title: {
				text: "Clicks"
			}
		},
		series: [{
            name: 'Clicks',
            data: [ 106, 49, 71]
        }],
		plotOptions: {
			series: {
				enableMouseTracking: true,
				shadow: false,
				animation: false
			} 
		},
		legend: {
			verticalAlign: 'top',
			borderColor: "#ddd",
			itemStyle: {
				color: "#888"
			}
		},
		credits: { enabled: false },
	});    
}

function renderCharts(){
	renderDailyChart();
	renderCostChart();
	renderCPCChart();
	renderNetworkChart();
	renderCampaignChart();
}

$(document).ready(function(){
	renderCharts();
});